import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TerimaOrderPage } from './terima-order.page';

const routes: Routes = [
  {
    path: '',
    component: TerimaOrderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TerimaOrderPageRoutingModule {}
