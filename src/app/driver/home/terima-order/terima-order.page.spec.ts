import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TerimaOrderPage } from './terima-order.page';

describe('TerimaOrderPage', () => {
  let component: TerimaOrderPage;
  let fixture: ComponentFixture<TerimaOrderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerimaOrderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TerimaOrderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
