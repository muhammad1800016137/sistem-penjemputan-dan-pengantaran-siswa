import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TerimaOrderPageRoutingModule } from './terima-order-routing.module';

import { TerimaOrderPage } from './terima-order.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TerimaOrderPageRoutingModule
  ],
  declarations: [TerimaOrderPage]
})
export class TerimaOrderPageModule {}
