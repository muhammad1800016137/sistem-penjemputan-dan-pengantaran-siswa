import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-terima-order',
  templateUrl: './terima-order.page.html',
  styleUrls: ['./terima-order.page.scss'],
})
export class TerimaOrderPage implements OnInit {

  map: L.Map;

  constructor(public router: Router,
    public auth: AngularFireAuth,
    public db: AngularFirestore) { }

  ngOnInit() {
    this.loadMap();

  }

  getOrder() {

  }

  loadMap() {
    this.map = L.map('map4', {
      center: [-7.8068289, 110.3828864],
      zoom: 14,
      renderer: L.canvas()
    });

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYXpoYXJyaXpraSIsImEiOiJja2lzeXY2MzEwbGxwMnlyeDhyYnlpZGN1In0.9ksfJbvVNVzreolZPkt5Ag', {
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1IjoiYXpoYXJyaXpraSIsImEiOiJja2lzeXY2MzEwbGxwMnlyeDhyYnlpZGN1In0.9ksfJbvVNVzreolZPkt5Ag'
    }).addTo(this.map);
  }

  tolak() {
    this.router.navigate(['/driver/home']);
  }

  terima() {
    this.router.navigate(['/pickup']);
  }

}
