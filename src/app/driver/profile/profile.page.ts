import { UbahDriverComponent } from './ubah-driver/ubah-driver.component';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {


  user: any = {};
  userData: any = {};
  constructor(
    public auth: AngularFireAuth,
    public db: AngularFirestore,
    public modal: ModalController,
    public router: Router,
    public toast: ToastController
  ) { }

  ngOnInit() {
    this.auth.onAuthStateChanged(res => {
      this.userData = res;
      this.getUser(res.email);
    });
  }

  getUser(email) {
    this.db.collection('users').doc(email).valueChanges().subscribe(res => {
      this.user = res;
    });
  }

  async pesan(message, color) {
    const toast = await this.toast.create({
      message,
      duration: 1000,
      color,
      position: 'top'
    });
    toast.present();
  }

  async Ubah() {
    const modal = await this.modal.create({
      component: UbahDriverComponent,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  signOut() {
    this.auth.signOut().then(() => {
      this.router.navigate(['/login']);
    }).catch((error) => {
      this.pesan('Gagal melakukan Logout', 'danger');
    });
  }


}
