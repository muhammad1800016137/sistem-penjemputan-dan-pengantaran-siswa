import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { ModalController, ToastController } from '@ionic/angular';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ubah-driver',
  templateUrl: './ubah-driver.component.html',
  styleUrls: ['./ubah-driver.component.scss'],
})
export class UbahDriverComponent implements OnInit {


  constructor(public modal: ModalController,
    public fsAuth: AngularFireAuth,
    public router: Router,
    public toast: ToastController
  ) { }

  @Input() user: any;
  @Input() email: any;
  password: any = {};

  userAuth: any;

  loading: boolean;
  data: any = {};
  peringatan: boolean;

  ngOnInit() { }

  ionViewDidEnter() {
    this.fsAuth.onAuthStateChanged((user) => {
      if (user) {
        this.userAuth = user;
      }
    });
  }

  cek() {
    if (this.data.password !== this.data.password2) {

    } else if (this.data.password === this.data.password2) {

    }
  }

  async pesan(message, color) {
    const toast = await this.toast.create({
      message,
      duration: 2000,
      color,
      position: 'top'
    });
    toast.present();
  }

  saveData() {
    this.loading = true;
    this.userAuth.updatePassword(this.data.password).then(res => {
      this.modal.dismiss();
      this.fsAuth.signOut();
      this.router.navigate(['/login']);
    }).catch(err => {
      this.pesan('Tidak dapat menyimpan data, coba lagi.', 'danger');
    });
  }

  close() {
    this.modal.dismiss();
  }

}
