import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import firebase from 'firebase/app';


@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    constructor(
        public router: Router,
        public alert: AlertController,
        public auth: AngularFireAuth,
        public toast: ToastController,
        public db: AngularFirestore
    ) { }

    user: any = {};
    loading: boolean;

    // routerControl(data) {
    //     if (data.peran === 'user') { this.router.navigate(['/user/home']); }
    //     if (data.peran === 'driver') { this.router.navigate(['/driver/home']); }
    // }
    data: any = {};

    ngOnInit() {
    }

    daftarUser() {
        this.router.navigate(['/register']);
    }
    daftarDriver() {
        this.router.navigate(['/register-driver']);
    }

    lupasandi() {
        this.router.navigate(['/lupasandi']);
    }

    async pesan(message, color) {
        const toast = await this.toast.create({
            message,
            duration: 2000,
            color,
            position: 'top'
        });
        toast.present();
    }

    login() {
        this.loading = true;
        this.auth.signInWithEmailAndPassword(this.user.email, this.user.password).then(res => {
            this.cekPeran(res.user.email);
        }).catch(err => {
            this.pesan('Silahkan cek ulang email / password anda', 'danger');
            this.loading = false;
        });
    }

    async cekPeran(email) {
        this.db.collection('users').doc(email).get().subscribe(res => {
            this.routerControl(res.data());
        });
    }

    routerControl(data) {
        if (data.peran === 'user') {
            this.router.navigate(['/user/home']);
        }
        if (data.peran === 'driver') {
            this.router.navigate(['/driver/home']);
        }
    }

}
