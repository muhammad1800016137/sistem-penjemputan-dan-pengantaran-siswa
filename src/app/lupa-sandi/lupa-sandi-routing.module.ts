import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LupaSandiPage } from './lupa-sandi.page';

const routes: Routes = [
  {
    path: '',
    component: LupaSandiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LupaSandiPageRoutingModule { }
