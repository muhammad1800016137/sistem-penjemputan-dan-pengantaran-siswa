import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ChatComponent } from './chat/chat.component';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';


@Component({
  selector: 'app-cari-driver',
  templateUrl: './cari-driver.page.html',
  styleUrls: ['./cari-driver.page.scss'],
})
export class CariDriverPage implements OnInit {


  map: L.Map;
  order: any = {};
  orderData: any = {};


  constructor(public router: Router,
    public modal: ModalController,
    public auth: AngularFireAuth,
    public db: AngularFirestore) { }

  ngOnInit() {
    this.loadMap();
    this.auth.onAuthStateChanged(res => {
      this.orderData = res;
      this.getOrder(res.email);
    });
  }

  getOrder(email) {
    this.db.collection('users').doc(email).valueChanges().subscribe(res => {
      this.orderData = res;
    });
  }

  loadMap() {
    this.map = L.map('map1', {
      center: [-7.8068289, 110.3828864],
      zoom: 14,
      renderer: L.canvas()
    });

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYXpoYXJyaXpraSIsImEiOiJja2lzeXY2MzEwbGxwMnlyeDhyYnlpZGN1In0.9ksfJbvVNVzreolZPkt5Ag', {
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1IjoiYXpoYXJyaXpraSIsImEiOiJja2lzeXY2MzEwbGxwMnlyeDhyYnlpZGN1In0.9ksfJbvVNVzreolZPkt5Ag'
    }).addTo(this.map);

  }

  kembali() {

    this.router.navigate(['/user/home']);
  }

  async showModal() {
    const modal = await this.modal.create({
      component: ChatComponent,
      cssClass: 'my-custom-class',
    });
    return await modal.present();
  }

  selesai() {
    this.router.navigate(['/pesan/review']);
  }

}
