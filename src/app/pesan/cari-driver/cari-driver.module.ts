import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CariDriverPageRoutingModule } from './cari-driver-routing.module';

import { CariDriverPage } from './cari-driver.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CariDriverPageRoutingModule
  ],
  declarations: [CariDriverPage]
})
export class CariDriverPageModule {}
