import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CariDriverPage } from './cari-driver.page';

const routes: Routes = [
  {
    path: '',
    component: CariDriverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CariDriverPageRoutingModule {}
