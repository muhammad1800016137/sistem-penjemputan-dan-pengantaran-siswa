import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CariDriverPage } from './cari-driver.page';

describe('CariDriverPage', () => {
  let component: CariDriverPage;
  let fixture: ComponentFixture<CariDriverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CariDriverPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CariDriverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
