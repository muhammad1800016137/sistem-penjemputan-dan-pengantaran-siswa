import { MapboxService } from './../services/mapbox.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoadingController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as L from 'leaflet';
import * as mapboxgl from 'mapbox-gl';
import * as MapboxDirections from '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions';

@Component({
    selector: 'app-pesan',
    templateUrl: './pesan.page.html',
    styleUrls: ['./pesan.page.scss'],
})
export class PesanPage implements OnInit {

    ID: any;
    docID: any;

    userdata: any = {};

    data: any = {};

    map: L.Map;

    constructor(public router: Router,
        public auth: AngularFireAuth,
        public db: AngularFirestore,
        public loading: LoadingController,
        public toast: ToastController,
        public routes: ActivatedRoute
    ) { }

    ngOnInit() {
        this.loadMap();
        // this.ID = this.routes.snapshot.paramMap.get('ID');
        // if (this.ID === 0) {
        //     this.docID = new Date().getTime().toString();
        // }
        // else {
        //     this.getData();
        // }
        this.auth.onAuthStateChanged(res => {
            this.userdata = res;
        });
    }

    // getData() {
    //     this.db.collection('orders').doc(this.ID).get().subscribe(res => {
    //         this.data = res.data();
    //     });
    // }

    loadMap() {
        // const directions = new MapboxDirections({
        //     accessToken: 'pk.eyJ1IjoiYXpoYXJyaXpraSIsImEiOiJja2lzeXY2MzEwbGxwMnlyeDhyYnlpZGN1In0.9ksfJbvVNVzreolZPkt5Ag',
        //     profile: 'mapbox/driving'
        // });

        // const map = new mapboxgl.Map({
        //     container: 'map',
        //     accessToken: 'pk.eyJ1IjoiYXpoYXJyaXpraSIsImEiOiJja2lzeXY2MzEwbGxwMnlyeDhyYnlpZGN1In0.9ksfJbvVNVzreolZPkt5Ag',
        //     style: 'mapbox://styles/mapbox/streets-v11',
        //     center: { lng: 110.600502, lat: -7.703403 },
        //     zoom: 13
        // });

        // map.addControl(directions, 'bottom-left');

        this.map = L.map('map', {
            center: [-7.8068289, 110.3828864],
            zoom: 14,
            renderer: L.canvas()
        });

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYXpoYXJyaXpraSIsImEiOiJja2lzeXY2MzEwbGxwMnlyeDhyYnlpZGN1In0.9ksfJbvVNVzreolZPkt5Ag', {
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1IjoiYXpoYXJyaXpraSIsImEiOiJja2lzeXY2MzEwbGxwMnlyeDhyYnlpZGN1In0.9ksfJbvVNVzreolZPkt5Ag'
        }).addTo(this.map);
    }

    kembali() {
        this.router.navigate(['/user/home']);
    }

    pesan() {

        this.data.pemesanan = this.userdata.email;
        this.loadings().then(() => {
            this.db.collection('orders').doc(this.docID).set(this.data).then(res => {
                this.router.navigate(['/pesan/cari-driver']);
            });
        });

        // if (this.ID === 0) {
        //     this.data.pemesan = this.userdata.email;
        //     this.data.data_created = new Date();
        //     this.db.collection('orders').doc(this.docID).set(this.data).then(res => {
        //         this.router.navigate(['/pesan/cari-driver']);
        //     });
        // }
        // else {
        //     this.db.collection('orders').doc(this.ID).update(this.data).then(res => {
        //         this.router.navigate(['/pesan/cari-driver']);
        //         this.message('Berhasil mendapatkan driver', 'top');
        //     });
        // }
    }

    async message(message, color) {
        const toast = await this.toast.create({
            message,
            duration: 1000,
            color,
            position: 'top'
        });
        toast.present();
    }

    async loadings() {
        const loading = await this.loading.create({
            cssClass: 'my-custom-class',
            message: 'Sedang mencari driver...',
            duration: 750,
            spinner: 'bubbles'
        });
        await loading.present();
    }

    voucher() {
        this.router.navigate(['/user/voucher']);
    }
}
