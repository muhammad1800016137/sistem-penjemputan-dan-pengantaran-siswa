import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-review',
  templateUrl: './review.page.html',
  styleUrls: ['./review.page.scss'],
})
export class ReviewPage implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  kirim() {
    this.router.navigate(['/user/home']);
  }

}
