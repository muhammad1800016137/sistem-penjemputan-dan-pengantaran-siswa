
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PesanPage } from './pesan.page';

const routes: Routes = [
  {
    path: '',
    component: PesanPage
  },
  {
    path: 'cari-driver',
    loadChildren: () => import('./cari-driver/cari-driver.module').then(m => m.CariDriverPageModule)
  },
  {
    path: 'review',
    loadChildren: () => import('./review/review.module').then(m => m.ReviewPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PesanPageRoutingModule { }
