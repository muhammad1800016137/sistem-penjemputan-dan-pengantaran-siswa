import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import { ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-register-driver',
  templateUrl: './register-driver.page.html',
  styleUrls: ['./register-driver.page.scss'],
})
export class RegisterDriverPage implements OnInit {

  driver: any = {};

  constructor(public route: Router,
    public auth: AngularFireAuth,
    public db: AngularFirestore,
    public toastController: ToastController,
    public router: Router) {
  }
  loading: boolean;

  ngOnInit() {
  }

  async pesan(message, color) {
    const toast = await this.toastController.create({
      message,
      duration: 1000,
      color,
      position: 'top'
    });
    toast.present();
  }

  daftar() {
    this.auth.createUserWithEmailAndPassword(this.driver.email, this.driver.password).then(rest => {
      this.createDriver(rest.user.email);
    }, err => {
      this.loading = false;
      this.pesan('Maaf email sudah dipakai', 'danger');
    }
    );
  }

  createDriver(email) {
    this.loading = true;
    this.db.collection('users').doc(email).set({
      peran: 'driver',
      nama: this.driver.username,
      email: this.driver.email,
      nohp: this.driver.nohp,
      password: this.driver.password,
      nopol: this.driver.nopol,
      alamat: this.driver.alamat,
      jenisKendaraan: this.driver.jenisKendaraan,
    }).then(res => {

      this.pesan('Pendaftaran Berhasil!', 'success');
      this.router.navigate(['/login']);
    }, err => {
      this.loading = false;
      this.pesan('Maaf pendaftaran anda gagal', 'danger');
    });
  }



  kembali() {
    this.router.navigate(['/login']);
  }

}
