import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PesanPage } from 'src/app/pesan/pesan.page';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  },
  {
    path: 'pesan',
    component: PesanPage,
    redirectTo: '/pesan'
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule { }
