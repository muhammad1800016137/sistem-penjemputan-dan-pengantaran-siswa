import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(public router: Router,
    public modal: ModalController,
    public auth: AngularFireAuth,
    public db: AngularFirestore, ) { }

  user: any = {};
  userData: any = {};

  ngOnInit() {
    this.auth.onAuthStateChanged(res => {
      this.userData = res;
      this.getUser(res.email);
    });
  }

  getUser(email) {
    this.db.collection('users').doc(email).valueChanges().subscribe(res => {
      this.user = res;
    });
  }

  pesan() {
    this.router.navigate(['/pesan']);
  }

  // async showModal() {
  //   const modal = await this.modal.create({
  //     component: OrderComponent,
  //     cssClass: 'my-custom-class',
  //   });
  //   return await modal.present();
  // }
}
