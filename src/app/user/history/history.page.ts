import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {


  history: any = {};
  historyData: any = {};
  constructor(
    public auth: AngularFireAuth,
    public db: AngularFirestore,
    public router: Router,
    public toast: ToastController
  ) { }

  ngOnInit() {
    this.auth.onAuthStateChanged((user) => {
      if (user) {
        this.history.uid = user.uid;
      }

    });
  }

  getHistory(email) {
    this.db.collection('historys').doc(email).valueChanges().subscribe(res => {
      this.history = res;
    });
  }

}
