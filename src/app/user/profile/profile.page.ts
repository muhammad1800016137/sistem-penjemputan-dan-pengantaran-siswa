import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ModalController, ToastController } from '@ionic/angular';
import { UbahPage } from './ubah/ubah.page';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Plugins, CameraResultType } from '@capacitor/core';
import { ImageUploaderPage } from 'src/app/image-uploader/image-uploader.page';
const { Camera } = Plugins;


@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
    constructor(
        public auth: AngularFireAuth,
        public db: AngularFirestore,
        public router: Router,
        public modal: ModalController,
        public toast: ToastController
    ) { }


    user: any = {};
    userData: any = {};

    updatingProfileImage: boolean;

    ngOnInit() {
        this.auth.onAuthStateChanged(res => {
            this.userData = res;
            this.getUser(res.email);
        });
    }

    getUser(email) {
        this.db.collection('users').doc(email).valueChanges().subscribe(res => {
            this.user = res;
        });
    }

    async pesan(message, color) {
        const toast = await this.toast.create({
            message,
            duration: 1000,
            color,
            position: 'top'
        });
        toast.present();
    }

    async takePicture() {
        const image = await Camera.getPhoto({
            quality: 90,
            allowEditing: true,
            resultType: CameraResultType.DataUrl
        });
        const imageUrl = image.dataUrl;
        const imagePath = this.userData.email + '/profile.png';
        const modal = await this.modal.create({
            component: ImageUploaderPage,
            cssClass: 'my-custom-class',
            componentProps: { imageData: imageUrl, imagePath, ratio: 1, width: 100, height: 100 }
        });
        modal.onDidDismiss().then(res => {
            if (res.data.imageUrl !== false) {
                this.updateUserData({ profileUrl: { url: res.data.imageUrl, ref: imagePath } });
                this.updateProfileUrl(res.data.imageUrl);
            }
        });
        return await modal.present();
    }

    updateUserData(data) {
        this.updatingProfileImage = true;
        this.db.collection('users').doc(this.userData.email).update(data).then(res => {
            this.updatingProfileImage = false;
        }).catch(err => {
            this.updatingProfileImage = false;
            this.pesan('Tidak dapat mengganti foto profil, coba lagi.', 'danger');
        });
    }

    updateProfileUrl(url) {
        this.auth.onAuthStateChanged(res => {
            res.updateProfile({ photoURL: url });
        });
    }

    async Ubah() {
        const modal = await this.modal.create({
            component: UbahPage,
            cssClass: 'my-custom-class'
        });
        return await modal.present();
    }

    signOut() {
        // [START auth_sign_out]
        this.auth.signOut().then(res => {
            this.router.navigate(['/login']);
        }).catch((error) => {
            this.pesan('Logout gagal', 'danger');
        });
        // [END auth_sign_out]
    }

}
