import { ModalController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
    selector: 'app-ubah',
    templateUrl: './ubah.page.html',
    styleUrls: ['./ubah.page.scss'],
})
export class UbahPage implements OnInit {

    constructor(public modal: ModalController,
        public fsAuth: AngularFireAuth,
        public router: Router,
        public toast: ToastController
    ) { }

    user: any;
    email: any;

    userAuth: any;

    loading: boolean;
    data: any = {};
    peringatan: boolean;

    ngOnInit() { }

    ionViewDidEnter() {
        this.fsAuth.onAuthStateChanged((user) => {
            if (user) {
                this.userAuth = user;
            }
        });
    }

    cek() {
        if (this.data.password !== this.data.password2) {

        } else if (this.data.password === this.data.password2) {

        }
    }

    async pesan(message, color) {
        const toast = await this.toast.create({
            message,
            duration: 2000,
            color,
            position: 'top'
        });
        toast.present();
    }

    saveData() {
        this.loading = true;
        this.userAuth.updatePassword(this.data.password).then(res => {
            this.modal.dismiss();
            this.fsAuth.signOut();
            this.router.navigate(['/login']);
        }).catch(err => {
            this.pesan('Tidak dapat menyimpan data, coba lagi.', 'danger');
        });
    }

    close() {
        this.modal.dismiss();
    }
}
