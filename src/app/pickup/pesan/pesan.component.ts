import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pesan',
  templateUrl: './pesan.component.html',
  styleUrls: ['./pesan.component.scss'],
})
export class PesanComponent implements OnInit {

  constructor(public modal: ModalController) { }

  ngOnInit() { }

  close() {
    this.modal.dismiss();
  }

}
