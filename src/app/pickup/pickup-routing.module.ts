import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PickupPage } from './pickup.page';

const routes: Routes = [
  {
    path: '',
    component: PickupPage
  },
  {
    path: 'selesai',
    loadChildren: () => import('./selesai/selesai.module').then(m => m.SelesaiPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PickupPageRoutingModule { }
