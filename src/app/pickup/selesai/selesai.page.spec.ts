import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelesaiPage } from './selesai.page';

describe('SelesaiPage', () => {
  let component: SelesaiPage;
  let fixture: ComponentFixture<SelesaiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelesaiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelesaiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
