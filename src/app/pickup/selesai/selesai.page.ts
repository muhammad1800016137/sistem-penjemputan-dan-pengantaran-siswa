import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-selesai',
  templateUrl: './selesai.page.html',
  styleUrls: ['./selesai.page.scss'],
})
export class SelesaiPage implements OnInit {

  map: L.Map;

  constructor(public router: Router) { }

  ngOnInit() {
    this.loadMap();
  }

  loadMap() {
    this.map = L.map('map5', {
      center: [-7.8068289, 110.3828864],
      zoom: 14,
      renderer: L.canvas()
    });

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYXpoYXJyaXpraSIsImEiOiJja2lzeXY2MzEwbGxwMnlyeDhyYnlpZGN1In0.9ksfJbvVNVzreolZPkt5Ag', {
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1IjoiYXpoYXJyaXpraSIsImEiOiJja2lzeXY2MzEwbGxwMnlyeDhyYnlpZGN1In0.9ksfJbvVNVzreolZPkt5Ag'
    }).addTo(this.map);
  }

  selesai() {
    this.router.navigate(['/driver/home']);
  }
}
