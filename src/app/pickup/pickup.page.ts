import { ModalController } from '@ionic/angular';
import { PesanComponent } from './pesan/pesan.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';


@Component({
  selector: 'app-pickup',
  templateUrl: './pickup.page.html',
  styleUrls: ['./pickup.page.scss'],
})
export class PickupPage implements OnInit {

  orderData: any = {};
  order: any = {};
  constructor(public router: Router,
    public auth: AngularFireAuth,
    public db: AngularFirestore,
    public modal: ModalController) { }

  ngOnInit() {
    this.auth.onAuthStateChanged(res => {
      this.orderData = res;
      this.getUser();
    });
  }

  getUser() {
    this.db.collection('orders').doc('idOrder').valueChanges().subscribe(res => {
      this.order = res;
    });
  }

  cancel() {
    this.router.navigate(['/driver/home']);
  }

  pickup() {
    this.router.navigate(['/pickup/selesai']);
  }

  pesan() {

  }

  async showModal() {
    const modal = await this.modal.create({
      component: PesanComponent,
      cssClass: 'my-custom-class',
    });
    return await modal.present();
  }

}
